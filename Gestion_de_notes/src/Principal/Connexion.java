package Principal;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Connection;

public class Connexion {
	private String URL;
	private String username;
	private String password;
	private String driverClasseName;
	
	
	
	public Connexion(String uRL, String username,String password, String driverClasseName) {
		super ();
		URL = uRL;
		this.username = username;
		this.password = password;
		this.driverClasseName = driverClasseName;
	}
	public Connection connectToDB() {
		try {
			Connection Connexion = DriverManager.getConnection(URL,username,password);
		
		return Connexion;
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	public String getURL() {
		return URL;
	}
	public void setURL(String uRL) {
		URL = uRL;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getDriverClasseName() {
		return driverClasseName;
	}
	public void setDriverClasseName(String driverClasseName) {
		this.driverClasseName = driverClasseName;
	}
	
	
	
	
	

}
